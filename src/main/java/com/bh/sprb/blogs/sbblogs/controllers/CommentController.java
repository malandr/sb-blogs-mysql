package com.bh.sprb.blogs.sbblogs.controllers;

import com.bh.sprb.blogs.sbblogs.model.Blog;
import com.bh.sprb.blogs.sbblogs.model.Comment;
import com.bh.sprb.blogs.sbblogs.model.User;
import com.bh.sprb.blogs.sbblogs.repositories.BlogRepository;
import com.bh.sprb.blogs.sbblogs.repositories.CommentRepository;
import com.bh.sprb.blogs.sbblogs.repositories.UserRepository;
import com.bh.sprb.blogs.sbblogs.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

@Controller
public class CommentController {

    private final CommentRepository commentRepository;
    private final UserService userService;
    private final BlogRepository blogRepository;

    public CommentController(CommentRepository commentRepository,
                             UserService userService,
                             BlogRepository blogRepository) {
        this.commentRepository = commentRepository;
        this.userService = userService;
        this.blogRepository = blogRepository;
    }

    @PostMapping("/create-comment")
    public String createNewPost(@Valid Comment comment, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {

            return "comment-form";

        } else {

            commentRepository.save(comment);

            return "redirect:/" + comment.getBlog().getId() + "/view";
        }
    }

    @GetMapping("/comment/{id}")
    public String commentPostWithId(@PathVariable Long id,
                                    Principal principal,
                                    Model model) {

        Optional<Blog> blog = blogRepository.findById(id);

        if (blog.isPresent()) {

//            Optional<User> user = userRepository.findByLastName(principal.getName());

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            User user = userService.findUserByLogin(auth.getName());

            if (user.getId() != null) {

                Comment comment = new Comment();

                comment.setUser(user);
                comment.setBlog(blog.get());

                model.addAttribute("comment", comment);

                return "comment-form";

            } else {
                return "/error";
            }

        } else {
            return "/error";
        }
    }
}