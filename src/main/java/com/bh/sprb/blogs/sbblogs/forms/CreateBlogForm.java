package com.bh.sprb.blogs.sbblogs.forms;

import com.bh.sprb.blogs.sbblogs.model.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class CreateBlogForm {

    @NotNull
    @Size(max=50)
    private String title;

    @NotNull
    @Size(max = 30)
    private String category;

    @NotNull
    @Size(max=2000)
    private String body;

    @NotNull
    @Size(min=2, max=20)
    private User author;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "CreateBlogForm{" +
                "title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", body='" + body + '\'' +
                ", author=" + author +
                '}';
    }
}
