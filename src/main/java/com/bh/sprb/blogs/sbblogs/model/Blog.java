package com.bh.sprb.blogs.sbblogs.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "blogs")
@Data
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "blog_id")
    private Long id;

    @Column(name = "title", nullable = false)
    @Length(min = 5, message = "*Your title must have at least 5 characters")
    @NotNull(message = "Please provide title")
    private String title;

    @Column(name = "body", columnDefinition = "TEXT")
    private String body;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", nullable = false, updatable = false)
    @CreationTimestamp
    private Date createDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
//    @NotNull
    private User user;

    @OneToMany(mappedBy = "blog", cascade = CascadeType.REMOVE)
    private Collection<Comment> comments;

    @Column(name = "category")
    private String category;

    public String getFormattedDate(Date createDate) {

        SimpleDateFormat formatter = new SimpleDateFormat("MMM, dd yyyy HH:mm:ss");

        String finalDate = formatter.format(createDate);

        return finalDate;
    }


}