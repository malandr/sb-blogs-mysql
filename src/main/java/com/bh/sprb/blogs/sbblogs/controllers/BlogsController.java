package com.bh.sprb.blogs.sbblogs.controllers;

import com.bh.sprb.blogs.sbblogs.forms.CreateBlogForm;
import com.bh.sprb.blogs.sbblogs.forms.FindBlogForm;
import com.bh.sprb.blogs.sbblogs.model.Blog;
import com.bh.sprb.blogs.sbblogs.model.User;
import com.bh.sprb.blogs.sbblogs.repositories.BlogRepository;
import com.bh.sprb.blogs.sbblogs.repositories.RoleRepository;
import com.bh.sprb.blogs.sbblogs.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.logging.Logger;

@Controller
public class BlogsController {

    private final Logger logger = Logger.getLogger(BlogsController.class.getName());

    private final BlogRepository blogRepository;
    private final UserService userService;
    private final RoleRepository roleRepository;

    public BlogsController(BlogRepository blogRepository, UserService userService, RoleRepository roleRepository) {
        this.blogRepository = blogRepository;
        this.userService = userService;
        this.roleRepository = roleRepository;
    }

    // LIST blogs

    @RequestMapping("/view-all")
    public String getBlogs(Model model) {

        Iterable<Blog> blogs = blogRepository.findAll();

        model.addAttribute("blogs", blogs);

        return "blogs";
    }

    // VIEW one blog

    @RequestMapping("/{id}/view")
    public String viewBlogById(@PathVariable Long id, Model model) {

        Blog blog = blogRepository.findById(id).get();

        model.addAttribute("blog", blog);

        return "blog";
    }

    // CREATE blog

    @GetMapping("/create-blog")
    public String createBlog(Model model) {

        model.addAttribute("blogForm", new CreateBlogForm());

        logger.info("========= Getting login page =========");

        return "create-blog";
    }

    @PostMapping("/create-blog")
    public String CreateBlog(@ModelAttribute CreateBlogForm blogForm) {

        User user;

        if (SecurityContextHolder.getContext().getAuthentication() != null){

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            user = userService.findUserByLogin(auth.getName());

        } else {

            user = userService.findUserByLogin("anma");
        }

        Blog blog = new Blog();

        blog.setTitle(blogForm.getTitle());
        blog.setCategory(blogForm.getCategory());
        blog.setBody(blogForm.getBody());
        blog.setCreateDate(new Date());
        blog.setUser(user);

        blogRepository.save(blog);

        logger.info("========= Blog created =========");

        return "blog-created";
    }

    // FIND blog

    @GetMapping("/find-blog")
    public String findBlogs(Model model) {

        model.addAttribute("findBlogForm", new FindBlogForm());

        return "find-blog";
    }

    @PostMapping("/find-blog")
    public String findBlogs(@ModelAttribute FindBlogForm findBlogForm, Model model) {

        Iterable<Blog> foundByTitleBlogs = blogRepository.findByTitle(findBlogForm.getTitle());

        model.addAttribute("foundByTitleBlogs", foundByTitleBlogs);

        for (Blog blog : foundByTitleBlogs) {

            System.out.println(blog.getTitle());
        }

//        List<Blog> foundByBodyBlogs = blogRepository.findByBody(findBlogForm.getBody());
//
//        model.addAttribute("foundByBodyBlogs", foundByBodyBlogs);

        return "found-blogs";

    }



}
