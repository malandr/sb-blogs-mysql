package com.bh.sprb.blogs.sbblogs.controllers;

import com.bh.sprb.blogs.sbblogs.model.User;
import com.bh.sprb.blogs.sbblogs.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.logging.Logger;

@Controller
public class HomeController {

    private final Logger logger = Logger.getLogger(HomeController.class.getName());
    private final UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/home")
    public String displayHomePage(Model model) {

        User user;

        if (SecurityContextHolder.getContext().getAuthentication() == null) {

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            user = userService.findUserByLogin(auth.getName());

        } else {

            user = userService.findUserByLogin("anma");

        }

        model.addAttribute("user", user);

        logger.info("==========================");   // log INFO-level message
        logger.info(user.getLogin());



        return "home";
    }


}
