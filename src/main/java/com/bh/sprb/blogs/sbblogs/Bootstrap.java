package com.bh.sprb.blogs.sbblogs;

import com.bh.sprb.blogs.sbblogs.model.User;
import com.bh.sprb.blogs.sbblogs.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements CommandLineRunner {

    private final UserService userService;

    public Bootstrap(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void run(String... args) throws Exception {
        loadData();
    }

    private void loadData() {

        User anma = new User();
        anma.setId(1000L);
        anma.setEmail("test@mail.com");
        anma.setLogin("anma");
        anma.setPassword("porkie");
        anma.setFirstName("Andrii");
        anma.setLastName("M");
        userService.saveUser(anma);

    }
}
