package com.bh.sprb.blogs.sbblogs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbBlogsApplication {

	public static void main(String[] args) {

		SpringApplication.run(SbBlogsApplication.class, args);
	}

}

