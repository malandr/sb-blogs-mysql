package com.bh.sprb.blogs.sbblogs.repositories;

import com.bh.sprb.blogs.sbblogs.model.Blog;
import com.bh.sprb.blogs.sbblogs.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    User findByLogin(String login);

    Optional<User> findByLastName(String lastName);
}
