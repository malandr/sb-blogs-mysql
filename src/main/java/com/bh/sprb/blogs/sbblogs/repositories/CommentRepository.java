package com.bh.sprb.blogs.sbblogs.repositories;

import com.bh.sprb.blogs.sbblogs.model.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
