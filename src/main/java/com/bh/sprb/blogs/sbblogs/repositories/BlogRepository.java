package com.bh.sprb.blogs.sbblogs.repositories;

import com.bh.sprb.blogs.sbblogs.model.Blog;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface BlogRepository extends CrudRepository<Blog, Long> {

    Iterable<Blog> findByTitle(String title);

    List<Blog> findByBody(String body);

    List<Blog> findByUser(String user);

}
