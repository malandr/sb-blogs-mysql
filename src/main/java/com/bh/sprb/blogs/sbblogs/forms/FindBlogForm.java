package com.bh.sprb.blogs.sbblogs.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FindBlogForm {

    @NotNull
    @Size(max = 30)
    private String title;

    private String body;

    private String user;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "FindBlogForm{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
