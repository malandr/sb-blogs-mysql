package com.bh.sprb.blogs.sbblogs.controllers;

import javax.validation.Valid;
import com.bh.sprb.blogs.sbblogs.model.User;
import com.bh.sprb.blogs.sbblogs.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;

@Controller
public class LoginController {

    private Logger logger = Logger.getLogger(LoginController.class.getName());

    @Autowired
    private UserService userService;

    @RequestMapping(value={"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login() {

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("login");

        logger.info("=======================");
        logger.info("Getting Login page");

        return modelAndView;
    }


}